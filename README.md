# Fullstack Developer Chalenge


## How To Participate

1. Fork this repo
    - To clone this repository you will need [GIT-LFS](https://git-lfs.github.com/)
2. Do your best
3. Prepare pull request and let us know that you are done

## Requirements

- Use PHP framework of your choice. We love Laravel, but you can chalange us :)
- Use JS framework (Vue, React, Angular).
- Use CSS preprocessor (SCSS, SASS, LESS).
- Use task-runner, to compile your css/js (Gulp, Webpack or Grunt), but do not commit the build.

## Some things we love
- API-first approach
- Structure
- Comments
- Meaningful methods
- Fun / Magic / Creativity!
